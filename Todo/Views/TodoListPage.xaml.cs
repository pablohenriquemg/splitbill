﻿using System;
using System.Diagnostics;
using Todo.Views;
using Xamarin.Forms;

namespace Todo
{
	public partial class TodoListPage : ContentPage
	{
		public TodoListPage()
		{
			InitializeComponent();
		}

		protected override async void OnAppearing()
		{
			base.OnAppearing();

			// Reset the 'resume' id, since we just want to re-start here
			((App)App.Current).ResumeAtTodoId = -1;
			listView.ItemsSource = await App.DatabaseItem.GetItemsAsync();
		}

		async void OnItemAdded(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new TodoItemPage
			{
				BindingContext = new TodoItem()
			});
		}

		async void OnListItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
            
            if (e.SelectedItem != null)
            {
                await Navigation.PushAsync(new TodoItemPage
                {
                    BindingContext = e.SelectedItem as TodoItem
                });
            }
		}

        async void OnPersonAddClicked(object sender, EventArgs e)
        {
            Console.WriteLine("OnPersonAddClicked()");
            await Navigation.PushAsync(new PersonPage());
        }

        async void SplitBillClicked(object sender, EventArgs e)
        {
            var answer = await DisplayAlert("Split Bill", "Do you want Split Bill?", "Yes", "No");
            if (answer)
            {
                await Navigation.PushAsync(new SummaryPage());
            }
        }
    }
}
