﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Todo.Views
{

	public partial class PersonPage : ContentPage
	{
		public PersonPage ()
		{
			InitializeComponent ();
		}

        async void OnSaveClicked(object sender, EventArgs e)
        {
            var person = new Person();
            Entry personName = this.FindByName<Entry>("personName");
            person.Name = personName.Text;
            await App.DatabasePerson.SavePersonAsync(person);
            await Navigation.PushAsync(new TodoListPage());
        }
    }
}