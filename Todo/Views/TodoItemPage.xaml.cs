﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Todo.Models;
using Xamarin.Forms;

namespace Todo
{
	public partial class TodoItemPage : ContentPage
	{
        
		public TodoItemPage()
		{
            Console.WriteLine("TodoItemPage()");
			InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            Console.WriteLine("OnAppearing()");
            Picker picker = this.FindByName<Picker>("PickerPersons");
            List<Person> list = await App.DatabasePerson.GetPersonsAsync();
            picker.ItemsSource = list;
            picker.ItemDisplayBinding = new Binding("Name");
            picker.SetBinding(Picker.SelectedItemProperty, "PersonName");
        }

		async void OnSaveClicked(object sender, EventArgs e)
		{
			var todoItem = (TodoItem) BindingContext;
            Picker picker = this.FindByName<Picker>("PickerPersons");
            Person selectedPerson = (Person) picker.SelectedItem;
            todoItem.PersonName = selectedPerson.Name;
            await App.DatabaseItem.SaveItemAsync(todoItem);
			await Navigation.PopAsync();
		}

		async void OnDeleteClicked(object sender, EventArgs e)
		{
			var todoItem = (TodoItem) BindingContext;
			await App.DatabaseItem.DeleteItemAsync(todoItem);
			await Navigation.PopAsync();
		}

	}
}
