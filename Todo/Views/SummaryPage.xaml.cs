﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Todo.Views
{

	public partial class SummaryPage : ContentPage
	{
		public SummaryPage ()
		{
			InitializeComponent();
		}

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            Console.WriteLine("SummaryPage.OnAppearing()");

            Span totalBill = this.FindByName<Span>("totalBill");
            Span personsQtt = this.FindByName<Span>("personsQtt");
            Span eachTotal = this.FindByName<Span>("eachTotal");
            
            List<Person> listPerson = await App.DatabasePerson.GetPersonsAsync();
            personsQtt.Text = listPerson.Count.ToString();

            List<TodoItem> listItem = await App.DatabaseItem.GetItemsAsync();
            int total = 0;
            listItem.ForEach(delegate (TodoItem item)
            {
                total += item.Price;
            });
            totalBill.Text = "R$ " + total.ToString() + ",00";

            int result = total / listPerson.Count;
            eachTotal.Text = "R$ " + result.ToString() + ",00";

        }

        async void OnDestroyClicked(object sender, EventArgs e)
        {
            Console.WriteLine("SummaryPage.OnDestroyClicked()");
            var answer = await DisplayAlert("Confirm", "Destroy data and start new Bill?", "Yes", "No");
            if (answer)
            {
                List<Person> listPerson = await App.DatabasePerson.GetPersonsAsync();
                listPerson.ForEach(delegate (Person p)
                {
                    App.DatabasePerson.DeletePersonAsync(p);
                });
                List<TodoItem> listItem = await App.DatabaseItem.GetItemsAsync();
                listItem.ForEach(delegate (TodoItem i)
                {
                    App.DatabaseItem.DeleteItemAsync(i);
                });
                await Navigation.PushAsync(new TodoListPage());
            }
        }
    }
}