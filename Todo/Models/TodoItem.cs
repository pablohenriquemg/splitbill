﻿using SQLite;

namespace Todo
{
	public class TodoItem
	{
		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
		public string Description { get; set; }
		public string PersonName { get; set; }
        public int Price { get; set; }
	}
}

