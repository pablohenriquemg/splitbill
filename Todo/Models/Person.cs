﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Todo.Models
{
    public class Person
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Name { get; set; }

        public Person()
        {
           
        }

        public Person(string Name)
        {
            this.Name = Name;
        }
    }
}
