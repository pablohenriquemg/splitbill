﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite;
using Todo.Models;

namespace Todo
{
	public class PersonDatabase
	{
		readonly SQLiteAsyncConnection database;

		public PersonDatabase(string dbPath)
		{
			database = new SQLiteAsyncConnection(dbPath);
			database.CreateTableAsync<Person>().Wait();
		}

		public Task<List<Person>> GetPersonsAsync()
		{
			return database.Table<Person>().ToListAsync();
		}

        public Task<Person> GetPersonAsync(int id)
		{
			return database.Table<Person>().Where(i => i.ID == id).FirstOrDefaultAsync();
		}

		public Task<int> SavePersonAsync(Person p)
		{
			if (p.ID != 0)
			{
				return database.UpdateAsync(p);
			}
			else {
				return database.InsertAsync(p);
			}
		}

		public Task<int> DeletePersonAsync(Person p)
		{
			return database.DeleteAsync(p);
		}
	}
}

