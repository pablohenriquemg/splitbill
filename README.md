Trabalho Prof. Carlos Augusto
====

Plataformas de Desenvolvimento de Aplicações Móveis.

Aplicativo básico para dividir a conta em um bar.

Das funcionalidades:

- Cadastro de pessoas.
- Cadastro de item(s) para cada pessoa.
- Fechar a conta com um total e valor a ser pago para cada um.

Está sendo utilizado SQLite database para Item e Pessoa.

Obtido exemplo atraves de um template da Microsoft : https://github.com/xamarin/xamarin-forms-samples/tree/master/Todo

Authors
-------

Pablo Dias, Joab Ferreira
